using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using ServiceStack;

namespace NukedBit.Sa;


public static class RenderTreeBuilderExtensions
{

    public static IBuilderComponent BuilderWithCounter(this RenderTreeBuilder builder) =>
        new BuilderWithCounter(builder);
}

public interface IBuilderComponent
{
    IBuilderAttribute OpenComponent(Type componentType);
    IBuilderAttribute OpenComponent<T>();
    IBuilderComponent CloseComponent();
}

public interface IBuilderAttribute
{
    public IBuilderAttribute AddAttributeIfValueNotNull(string name, string? value);
    public IBuilderAttribute AddAttributeIfValueNotNull(string name, object? value);
    public IBuilderAttribute AddAttributeIfValueNotNull(string name, bool? value);
    public IBuilderAttribute AddAttributeValueOrDefault(string name, string? value, string defaultValue);
    public IBuilderAttribute AddAttributeValueOrDefault(string name, object? value, object defaultValue);
    IBuilderAttribute AddAttribute(string name, object? value);
    IBuilderAttribute AddChildContent<TItem>(Action<IBuilderComponent, TItem> build);
}

public class BuilderWithCounter : IBuilderComponent, IBuilderAttribute
{
    private readonly RenderTreeBuilder _builder;
    private int _sequence = 0;

    internal BuilderWithCounter(RenderTreeBuilder builder)
    {
        _builder = builder;
    }
    
    internal BuilderWithCounter(RenderTreeBuilder builder, ref int sequence)
    {
        _builder = builder;
        _sequence = sequence;
    }
    
    
    /// <summary>
    /// Return new sequence or same if null
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="sequence"></param>
    /// <param name="name"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public IBuilderAttribute AddAttributeIfValueNotNull(string name,
        string? value)
    {
        if (value.IsNullOrEmpty())
        {
            return this;
        }
        
        _builder.AddAttribute(_sequence, name, value);

        _sequence += 1;
        return this;
    }
    
    /// <summary>
    /// Return new sequence or same if null
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="sequence"></param>
    /// <param name="name"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public IBuilderAttribute AddAttributeIfValueNotNull(string name, object? value)
    {
        if (value is null)
        {
            return this;
        }
        
        _builder.AddAttribute(_sequence, name, value);

        _sequence += 1;
        return this;
    }
    
    public IBuilderAttribute AddAttribute(string name, object? value)
    {
        if (value is null)
        {
            return this;
        }
        
        _builder.AddAttribute(_sequence, name, value);

        _sequence += 1;
        return this;
    }
    
    /// <summary>
    /// Return new sequence or same if null
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="sequence"></param>
    /// <param name="name"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public IBuilderAttribute AddAttributeIfValueNotNull(string name,
        bool? value)
    {
        if (value is null)
        {
            return this;
        }
        
        _builder.AddAttribute(_sequence, name, value.Value);
        _sequence += 1;
        return this;
    }

    public IBuilderAttribute AddAttributeValueOrDefault(string name, string? value, string defaultValue)
    {
        AddAttributeIfValueNotNull(name,
            value ?? defaultValue ?? throw new ArgumentNullException(nameof(defaultValue)));
        return this;
    }

    public IBuilderAttribute AddChildContent<TItem>(Action<IBuilderComponent, TItem> build) 
    {
        _builder.AddAttribute(_sequence, "ChildContent", (RenderFragment<object>)((context) =>
        {
            void Render(RenderTreeBuilder builder)
            {
                build(new BuilderWithCounter(builder, ref _sequence), (TItem)context);    
            }

            return Render;
        }));
        _sequence += 1;
        return this;
    }
   
    public IBuilderAttribute AddAttributeValueOrDefault(string name, object? value, object defaultValue)
    {
        AddAttributeIfValueNotNull(name,
            value ?? defaultValue ?? throw new ArgumentNullException(nameof(defaultValue)));
        return this;
    }

    public IBuilderAttribute OpenComponent(Type componentType)
    {
        _builder.OpenComponent(_sequence, componentType);
        _sequence += 1;
        return this;
    }
    
    public IBuilderAttribute OpenComponent<T>()
    {
        _builder.OpenComponent(_sequence, typeof(T));
        _sequence += 1;
        return this;
    }
    
    public IBuilderComponent CloseComponent()
    {
        _builder.CloseComponent();
        return this;
    }
}