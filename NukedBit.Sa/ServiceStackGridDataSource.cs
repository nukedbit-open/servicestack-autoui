using System.Collections;
using System.Text;
using DevExpress.Blazor;
using DevExpress.Data.Filtering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceStack;

namespace NukedBit.Sa;

public static class ConversionExtensions
{
    public static T OfTypeOrThrow<T>(this object obj)
    {
        if (obj is T t)
            return t;
        throw new InvalidCastException($"Cannot cast {obj.GetType().Name} to {typeof(T).Name}");
    }
}


public class ServiceStackCriteriaConverter
{
    private record struct QueryExtraArgs(string Property, string Value)
    {
        public static List<QueryExtraArgs> Empty = new();
        public static List<QueryExtraArgs> One(QueryExtraArgs args) => new() { args };
    }

    private readonly List<QueryExtraArgs> _args = new();

    private readonly Dictionary<Type, Func<CriteriaOperator, List<QueryExtraArgs>>> _converters;

    public ServiceStackCriteriaConverter()
    {
        _converters = new Dictionary<Type, Func<CriteriaOperator, List<QueryExtraArgs>>>()
        {
            [typeof(GroupOperator)] = Group,
            [typeof(BinaryOperator)] = Binary,
            [typeof(FunctionOperator)] = Function,
        };
    }

    public void AddCriteria(CriteriaOperator? @operator)
    {
        if (@operator is null) return;
        _args.AddRange(InvokeCriteriaBuilder(@operator));
    }

    public string Build()
    {
        if (!_args.Any()) return string.Empty;
        var sb = new StringBuilder();

        foreach (var arg in _args)
        {
            sb.AppendFormat("&{0}={1}", arg.Property, arg.Value);
        }

        return sb.ToString();
    }

    private List<QueryExtraArgs> Function(CriteriaOperator @operator)
    {
        var functionOperator = @operator.OfTypeOrThrow<FunctionOperator>();
        var operandProperty = functionOperator.Operands.OfType<OperandProperty>().FirstOrDefault();
        var operandPropertyValue = functionOperator.Operands.OfType<OperandValue>().FirstOrDefault();
        if (operandProperty is null || operandPropertyValue is null)
        {
            return QueryExtraArgs.Empty;
        }

        return QueryExtraArgs.One(new QueryExtraArgs($"{operandProperty.PropertyName}Contains",
            operandPropertyValue.Value.ToString()));
    }

    private List<QueryExtraArgs> Binary(CriteriaOperator @operator)
    {
        var binaryOperator = @operator.OfTypeOrThrow<BinaryOperator>();
        var operandProperty = binaryOperator.LeftOperand as OperandProperty;
        var operandPropertyValue = binaryOperator.RightOperand as OperandValue;
        if (operandProperty is null || operandPropertyValue is null)
        {
            return QueryExtraArgs.Empty;
        }

        if (binaryOperator.OperatorType == BinaryOperatorType.Equal)
        {
            return QueryExtraArgs.One(new QueryExtraArgs($"{operandProperty.PropertyName}",
                operandPropertyValue.Value.ToString()));
        }

        return QueryExtraArgs.Empty;
    }

    private List<QueryExtraArgs> InvokeCriteriaBuilder(CriteriaOperator @operator)
    {
        if (_converters.TryGetValue(@operator.GetType(), out var converter))
        {
            return converter(@operator);
        }

        return QueryExtraArgs.Empty;
    }

    private List<QueryExtraArgs> Group(CriteriaOperator @operator)
    {
        var groupOperator = @operator.OfTypeOrThrow<GroupOperator>();
        return groupOperator.Operands.SelectMany(InvokeCriteriaBuilder).ToList();
    }
}


public class ServiceStackGridDataSource<TRequest, TResponse> : GridCustomDataSource
    where TRequest : IReturn<QueryResponse<TResponse>>, IQueryDb
{
    private readonly Func<TRequest> _queryFactory;
    private readonly IHttpRestClientAsync _clientAsync;
    private readonly ConfiguredType _configuredType;
    private ILogger _logger;

    public ServiceStackGridDataSource(Func<TRequest> queryFactory, IHttpRestClientAsync clientAsync, ConfiguredType configuredType, ILogger logger)
    {
        _queryFactory = queryFactory ?? throw new ArgumentNullException(nameof(queryFactory));
        _clientAsync = clientAsync ?? throw new ArgumentNullException(nameof(clientAsync));
        _configuredType = configuredType ?? throw new ArgumentNullException(nameof(configuredType));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }
            
    // IReturn<QueryResponse<T>>, 
    public override async Task<int> GetItemCountAsync(GridCustomDataSourceCountOptions options, CancellationToken cancellationToken)
    {
        var queryDb = _queryFactory.Invoke();
        
        queryDb.Take = 1; 

        var getUrl = queryDb.ToGetUrl();
        
        if (!getUrl.Contains("?"))
        {
            getUrl += "?";    
        }

        var builder = new ServiceStackCriteriaConverter();
        builder.AddCriteria(options.FilterCriteria);
        getUrl += builder.Build();
        
        var response = await _clientAsync.GetAsync<QueryResponse<TResponse>>(getUrl, cancellationToken);

        return response.Total;
    }

    /// <summary>
    /// Append a new filter to the existing filter criteria.
    /// </summary>
    /// <param name="by"></param>
    /// <param name="append"></param>
    /// <returns></returns>
    private string AppendOrderBy(string by, string append)
    {
        if (by.IsNullOrEmpty())
        {
            return append;
        }
        return $"{by}, {append}";
    }

    
    public override async Task<IList> GetItemsAsync(GridCustomDataSourceItemsOptions options,
        CancellationToken cancellationToken)
    {

        var queryDb = _queryFactory.Invoke();

        PopulateSortInfo(options, queryDb);
        

        Console.WriteLine("Filter {0}", options.FilterCriteria?.GetType()?.Name ?? "");
        //
        Console.WriteLine("FILTER JSON");
        Console.WriteLine(JsonConvert.SerializeObject(options.FilterCriteria, new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All,
            Converters = { new Newtonsoft.Json.Converters.StringEnumConverter() }
        }));


        queryDb.Skip = options.StartIndex;
        queryDb.Take = options.Count;

        var getUrl = queryDb.ToGetUrl();
        
        if (!getUrl.Contains("?"))
        {
            getUrl += "?";    
        }
        
        var builder = new ServiceStackCriteriaConverter();
        builder.AddCriteria(options.FilterCriteria);
        getUrl += builder.Build();
        
        var response = await _clientAsync.GetAsync<QueryResponse<TResponse>>(getUrl, cancellationToken);

        return response.Results;
    }

    private void PopulateSortInfo(GridCustomDataSourceItemsOptions options, TRequest queryDb)
    {
        foreach (var sortInfo in options.SortInfo)
        {
            var fieldName = sortInfo.FieldName;
            var propertyOptions = _configuredType.GetPropertyOptions(fieldName);
            if (propertyOptions.ColumnOptions?.SortBy is {} sortBy)
            {
                fieldName = sortBy;
            }
            
            if(!(propertyOptions?.ColumnOptions.AllowSort ?? false)) continue;
            if (sortInfo.DescendingSortOrder)
            {
                queryDb.OrderByDesc = AppendOrderBy(queryDb.OrderByDesc, fieldName);
            }
            else
            {
                queryDb.OrderBy = AppendOrderBy(queryDb.OrderByDesc, fieldName);
            }
        }
    }
}