﻿using System.Linq.Expressions;
using System.Reflection;
using DevExpress.Blazor;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using ServiceStack;
using ServiceStack.Blazor;

namespace NukedBit.Sa;


public static class InputTypes
{
    public const string ComboBox = "combobox";
    public const string Text = "text";
    public const string TextArea = "textarea";
    public const string Date = "date";
    /// <summary>
    /// NumberFields and Masked values
    /// </summary>
    public const string Mask = "mask";
}

public class RelationshipOptions
{
    /// <summary>
    /// Default: Name
    /// </summary>
    public string TextFieldName { get; set; } = "Name";
    /// <summary>
    /// Default: Id
    /// </summary>
    public string ValueFieldName { get; set; } = "Id";
    public Func<BlazorComponentBase, object, QueryBase> ItemsSource { get; set; }

}

public record PropertyOptions
{
    public string Name { get; init; }
    public Type RecordType { get; init; }
    public ColumnOptions ColumnOptions { get; set; } = new();
    public InputOptions InputOptions { get; set; } = new();
    
    public PropertyInfo PropertyInfo => RecordType.GetProperty(Name);
    
}

public class ColumnOptions
{
    public bool Visible { get; set; } = true;
    public bool IsKeyFieldName { get; set; }
    public bool AllowSort { get; set; }
    public string MinWidth { get; set; }
    public string Width { get; set; }
    public bool? AllowGroup { get; set; }
    public string Label { get; set; }
    public string DisplayFormat { get; set; }
    public bool AllowEdit { get; set; } = true;
    
    public Func<object, string> ValueFormatter { get; set; }
    
    public RenderFragment<GridDataColumnCellDisplayTemplateContext> CellDisplayTemplate { get; set; }
    public RenderFragment<GridDataColumnCellEditTemplateContext> CellEditTemplate { get; set; } 
    public RenderFragment<GridColumnFooterTemplateContext> FooterTemplate { get; set; } 
    public GridColumnGroupInterval GroupInterval { get; set; } 
    public int? GroupIndex { get; set; }
    public int SortIndex { get; set; }
    public bool? ShowInColumnChooser { get; set; }
    public object? FilterRowValue { get; set; }
    
    
    /// <summary>
    /// Specify the related table field name, your QueryDb request must specify a join clause for this to work example
    /// class QueryGames : QueryDb<Game>, ILeftJoin<Game,GameStatus> {}
    /// Game Status Has a field Name, then you can sort  on that field by Specifying Name
    /// </summary>
    public string SortBy { get; set; }
}

public class InputOptions
{
    /// <summary>
    /// text, textarea, masked, spin
    /// </summary>
    public string? Type { get; set; }
    public SizeMode? SizeMode { get; set; }

    public MaskMode? MaskMode { get; set; }
    public string? Mask { get; set; }

    public RelationshipOptions RelationshipOptions { get; set; }
    
}

public class ConfiguredType
{
    public Type Type { get; }
    public IList<PropertyOptions> PropertyOptions { get; }
    
    private readonly Dictionary<string, PropertyOptions> _propertyOptions;

    public ConfiguredType(Type type, List<PropertyOptions> propertyOptions)
    {
        Type = type;
        PropertyOptions = propertyOptions;
        _propertyOptions = propertyOptions.ToDictionary(x => x.Name);
    }
    public PropertyOptions? GetPropertyOptions(string name)
    {
        return _propertyOptions.TryGetValue(name, out var propertyOptions) ? propertyOptions : null;
    }
}


public interface ISaTypeConfigurator<TRecord> where TRecord : class
{
    ISaTypeConfigurator<TRecord> Property<P>(Expression<Func<TRecord, P>> property, Action<PropertyOptions> options);
}

class SaTypeConfigurator<TRecord>: ISaTypeConfigurator<TRecord> where TRecord: class
{
    //  public static GridConfigurator<TRecord> New() => new();
    
    private List<PropertyOptions> _propertyOptions = new();

    public ISaTypeConfigurator<TRecord> Property<P>(Expression<Func<TRecord, P>> property, Action<PropertyOptions> options)
    {
        var expression = (MemberExpression)property.Body;
        var name = expression.Member.Name;
        var propertyOptions = new PropertyOptions()
        {
            RecordType = typeof(TRecord),
            Name = name
        };
        options(propertyOptions);
        _propertyOptions.Add(propertyOptions);
        return this;
    }
    
    public ConfiguredType Build() => new(typeof(TRecord), _propertyOptions);
}

public interface ISaBuilder
{
    ISaBuilder Configure<TRecord>(Action<ISaTypeConfigurator<TRecord>> config) where TRecord : class;
}

public interface ISaConfig
{
    ConfiguredType GetConfiguredType<TRecord>() where TRecord : class;
    ConfiguredType GetConfiguredType(Type type);
}

public class SaBuilder : ISaBuilder, ISaConfig
{
    private readonly Dictionary<Type, ConfiguredType> _configuredTypes = new();
    
    public ISaBuilder Configure<TRecord>(Action<ISaTypeConfigurator<TRecord>> configure) where TRecord : class
    {
        var cfg = new SaTypeConfigurator<TRecord>();
        configure(cfg);
        _configuredTypes.Add(typeof(TRecord), cfg.Build());
        return this;
    }

    public ConfiguredType GetConfiguredType<TRecord>() where TRecord : class
    {
        return GetConfiguredType(typeof(TRecord));
    }

    public ConfiguredType GetConfiguredType(Type type)
    {
        return _configuredTypes.TryGetValue(type, out var configuredType) ? configuredType : null;
    }
}

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureSa(this IServiceCollection services, Action<ISaBuilder> builder)
    {
        var saBuilder = new SaBuilder();
        builder(saBuilder);
        services.AddSingleton<ISaConfig>(saBuilder);
        return services;
    }
}