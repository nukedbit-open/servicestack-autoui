using System.Reflection;

namespace NukedBit.Sa;

public static class PropertyInfoExtensions
{
    public static object? GetPropertyValueIfNotNull(this PropertyInfo info, object? dataItem)
    {
        try
        {
            return dataItem is null ? null : info.GetValue(dataItem);
        }
        catch (Exception e)
        {
            return null;
        }
    }
}