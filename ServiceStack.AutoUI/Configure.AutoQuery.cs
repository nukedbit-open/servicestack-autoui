using Microsoft.AspNetCore.Hosting;
using ServiceStack;
using ServiceStack.Data;

[assembly: HostingStartup(typeof(ServiceStack.AutoUI.ConfigureAutoQuery))]

namespace ServiceStack.AutoUI
{
    public class ConfigureAutoQuery : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder) => builder
            .ConfigureServices(services => {
                // Enable Audit History
                services.AddSingleton<ICrudEvents>(c =>
                    new OrmLiteCrudEvents(c.Resolve<IDbConnectionFactory>()));
            })
            .ConfigureAppHost(appHost => {

                // For TodosService
                appHost.Plugins.Add(new AutoQueryDataFeature()); 

                // For Bookings https://github.com/NetCoreApps/BookingsCrud
                appHost.Plugins.Add(new AutoQueryFeature
                {
                    MaxLimit = 100,
                    IncludeTotal = true,
                });

                appHost.Resolve<ICrudEvents>().InitSchema();
            });
    }
}