using ServiceStack.AutoUI.ServiceModel;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Data;

[assembly: HostingStartup(typeof(ServiceStack.AutoUI.ConfigureDb))]

namespace ServiceStack.AutoUI
{
    public class ConfigureDb : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder) => builder
            .ConfigureServices((context,services) => services.AddSingleton<IDbConnectionFactory>(new OrmLiteConnectionFactory(
                context.Configuration.GetConnectionString("DefaultConnection") ?? ":memory:",
                SqliteDialect.Provider)))
            .ConfigureAppHost(appHost =>
            {
                // Create non-existing Table and add Seed Data Example
                using var db = appHost.Resolve<IDbConnectionFactory>().Open();          
                
                if (db.CreateTableIfNotExists<RoomLocation>())
                {
                    db.Save(new RoomLocation()
                    {
                        Name = "South",
                        CreatedBy = "manager@email.com",
                        CreatedDate = DateTime.UtcNow,
                        ModifiedBy = "manager@email.com",
                        ModifiedDate = DateTime.UtcNow,
                    });
                    
                    db.Save(new RoomLocation()
                    {
                        Name = "North",
                        CreatedBy = "manager@email.com",
                        CreatedDate = DateTime.UtcNow,
                        ModifiedBy = "manager@email.com",
                        ModifiedDate = DateTime.UtcNow,
                    });
                      
                    db.Save(new RoomLocation()
                    {
                        Name = "East",
                        CreatedBy = "manager@email.com",
                        CreatedDate = DateTime.UtcNow,
                        ModifiedBy = "manager@email.com",
                        ModifiedDate = DateTime.UtcNow,
                    });
                }
                
                if (db.CreateTableIfNotExists<Booking>())
                {
                    for (int i = 0; i < 500; i++)
                    {
                        db.CreateBooking($"Booking {i}", RoomType.Double, Random.Shared.Next(1, 100), 120, "manager@email.com", Random.Shared.Next(1, 3));    
                    }
                }

               
            });
    }

    public static class ConfigureDbUtils
    {
        static int bookingId = 0;
        public static void CreateBooking(this IDbConnection db, string name, RoomType type, int roomNo, decimal cost, string by, int roomLocation) =>
            db.Insert(new Booking
            {
                Id = ++bookingId,
                Name = name,
                RoomType = type,
                RoomNumber = roomNo,
                Cost = cost,
                BookingStartDate = DateTime.UtcNow.AddDays(bookingId),
                BookingEndDate = DateTime.UtcNow.AddDays(bookingId + 7),
                CreatedBy = by,
                CreatedDate = DateTime.UtcNow,
                ModifiedBy = by,
                ModifiedDate = DateTime.UtcNow,
                RoomLocationId = roomLocation
            });

    }
}
