using System;
using System.Net.Http;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Blazor.Extensions.Logging;
using DevExpress.Blazor;
using Humanizer;
using Microsoft.AspNetCore.Components.Rendering;
using NukedBit.Sa;
using ServiceStack.Blazor;
using ServiceStack.AutoUI.Client;
using ServiceStack.AutoUI.Client.Shared;
using ServiceStack.AutoUI.ServiceModel;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.Services.AddLogging(c => c
    .AddBrowserConsole()
    .SetMinimumLevel(LogLevel.Trace)
);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");
builder.Services.AddOptions();
builder.Services.AddAuthorizationCore();

// Use / for local or CDN resources
builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddScoped<AuthenticationStateProvider>(s => s.GetRequiredService<ServiceStackStateProvider>());
builder.Services.AddDevExpressBlazor(configure => configure.BootstrapVersion = BootstrapVersion.v5);
builder.Services.AddBlazorApiClient(builder.Configuration["ApiBaseUrl"] ?? builder.HostEnvironment.BaseAddress);

builder.Services.ConfigureSa(saBuilder =>
{
    saBuilder.Configure<Booking>((cfgRecord) =>
    {
        cfgRecord
            .Property(booking => booking.Name, options =>
            {
                options.ColumnOptions.Label = "Name";
                options.InputOptions.Type = "text";
                options.ColumnOptions.AllowSort = true;
            })
            .Property(booking => booking.CreatedDate, options =>
            {
                options.ColumnOptions.Label = "Elapsed Time";
                options.ColumnOptions.AllowEdit = false;
                options.InputOptions.Type = "text";
                options.ColumnOptions.AllowSort = true;
                options.ColumnOptions.ValueFormatter = (value) => (DateTime.UtcNow - ((Booking)value).CreatedDate).Humanize();
            })
            .Property(booking => booking.ModifiedDate, options =>
            {
                options.ColumnOptions.Label = "Modified At";
                options.ColumnOptions.AllowEdit = false;
                options.InputOptions.Type = "text";
                options.ColumnOptions.AllowSort = true;
                //options.ColumnOptions.ValueFormatter = (value) => (DateTime.UtcNow - ((Booking)value).CreatedDate).Humanize();
                options.ColumnOptions.CellDisplayTemplate = value =>
                {
                    void Build(RenderTreeBuilder rbuilder)
                    {
                        int i = 0;
                        rbuilder.OpenComponent<DemoDateCellComponent>(i);
                        rbuilder.AddAttribute(++i, nameof(DemoDateCellComponent.Date), value.Value);
                        rbuilder.CloseComponent();
                    }

                    return Build;
                };
            })
            .Property(booking => booking.Notes, options =>
            {
                options.ColumnOptions.Label = "Notes";
                options.InputOptions.Type = "textarea";
                options.InputOptions.SizeMode = SizeMode.Large;
            })
            .Property(booking => booking.RoomNumber, options =>
            {
                options.ColumnOptions.Label = "Room Number";
                options.ColumnOptions.AllowSort = true;
                options.InputOptions.Type = "mask";
                options.InputOptions.MaskMode = MaskMode.Numeric;
            })
            .Property(booking => booking.RoomType, options =>
            {
                options.InputOptions.Type = "combobox";
                options.ColumnOptions.Label = "Room Type";
            })
            .Property(booking => booking.RoomLocation, options =>
            {
                options.InputOptions.Type = "combobox"; 
                options.ColumnOptions.AllowSort = false;
                
                options.InputOptions.RelationshipOptions = new RelationshipOptions
                {
                    ItemsSource = (_, _) => new QueryRoomLocations(),
                    TextFieldName = "Name",
                    ValueFieldName = "Id"
                };
                options.ColumnOptions.Label = "Room Location";
                options.ColumnOptions.ValueFormatter = (value) => (value as Booking).RoomLocation.Name.ToString();
            });

    });
});

builder.Services.AddScoped<ServiceStackStateProvider>();

await builder.Build().RunAsync();